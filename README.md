## ggm-test-bench
expects ggm test instance on same host

# functionality testing
spawns different test workers:

* http post data producers
* http get data consumers
* zmq data lifecyle (insert, download, delete)
* (device_state_db observer)

```
# -c --cleanup
./ggm-test-bench -f (-c)
```

# performance testing
TODO...

* data throughput (message size)
* no of messages
* no of connections
* latency
* input/output performance under load

```
./ggm-test-bench -p
```
