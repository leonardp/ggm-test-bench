import sys

import psycopg2
import psycopg2.extras
from psycopg2 import sql

import time
import json
from pprint import pprint
from datetime import datetime as dt

with open(f'./config/timescale.secret', 'r') as raw:
    dbcfg = json.load(raw)
    dbname = dbcfg['dbname']
    dbuser = dbcfg['dbuser']
    dbpw = dbcfg['dbpw']
    dbhost = dbcfg['dbhost']
    del dbcfg

dsn = f'dbname={dbname} user={dbuser} password={dbpw} host={dbhost}'
conn = psycopg2.connect(dsn)
conn.set_isolation_level(0) # autocommit
cur = conn.cursor()

roles = ['functionality', 'performance']
for role in roles:
    with open(f'./config/{role}.json', 'r') as raw:
        cfg = json.load(raw)

    for k in cfg['tests']:
        if cfg['tests'][k]['enabled']:
            for i in range(0,cfg['tests'][k]['instances']):
                name = 'test_'+k+f'_{i}'

                query = sql.SQL("DROP schema {0} CASCADE").format(
                    sql.Identifier(name))

                cur.execute(query)
