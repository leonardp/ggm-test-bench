import os, zmq, requests
import asyncio

from testlib.kernel import TestKernel
from testlib.sqldb import SqlDB
from testlib.common_test import CommonTest

import random
from datetime import timedelta as td
from pprint import pprint
import time

class DataPerf(TestKernel, CommonTest, SqlDB):
    def __init__(self, *args, **kwargs):
        self.cleanup = kwargs['config']['cleanup']
        self.start_delay = (5,30)
        self.start_delay = (1,2)
        self.test_interval = (10,20)
        TestKernel.__init__(self, *args, **kwargs)
        CommonTest.__init__(self, *args, **kwargs)
        SqlDB.__init__(self, *args, **kwargs)

    async def cycle(self, testname, length, chunk_size):
        ggm = self.get_zmq_client()

        if self.cleanup:
            cmd = ['series', 'insert', self.name, testname, {'time': self.get_iso_timestamp(), 'tags': {'deleteable': 'xxx'}, 'fields': {}}]
            await ggm.psnd(cmd)
            ret = await ggm.precv()

            cmd = ['series', 'delete', self.name, testname]
            await ggm.psnd(cmd)
            ret = await ggm.precv()

            cmd = ['json', 'delete', 'device_state_db', self.name]
            await ggm.psnd(cmd)
            ret = await ggm.precv()

            self.glog.info(f'Cleaned up {self.name} - {testname}')
            return

        #await asyncio.sleep(*self.start_delay)
        self.glog.info(f'Starting {self.name} - {testname}')

        # update device_state db
        cmd = ['json', 'nupsert', 'device_state_db', self.name, {'seen': self.get_iso_timestamp(), 'dev_id': self.name}]
        await ggm.psnd(cmd)
        ret = await ggm.precv()
        self.glog.debug(f'upsert state db {ret}')

        while True:
            timer = time.time()
            transfered = 0
            while length > transfered:
                data = self.get_data('ccs811_i2c0')
                cmd = ['series', 'upload', self.name, testname, data]
                await ggm.psnd(cmd)
                ret = await ggm.precv()
                transfered += chunk_size
            print(f'upload took {time.time()-timer} seconds')

            await asyncio.sleep(*self.test_interval)


            # delete
            cmd = ['series', 'insert', self.name, testname, {'time': self.get_iso_timestamp(), 'tags': {'deleteable': 'xxx'}, 'fields': {}}]
            await ggm.psnd(cmd)
            ret = await ggm.precv()

            cmd = ['series', 'delete', self.name, testname]
            await ggm.psnd(cmd)
            ret = await ggm.precv()
            if not ret == {'Error': False, 'Reason': f'Deletion of {testname} successful.'}:
                self.glog.error(f'{self.name} - {testname} --- failed deleting: {ret}!')

            await asyncio.sleep(*self.test_interval)


def data_perf_process(cfg):
    cfg['base_dir'] = os.path.dirname(__file__)

    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    cfg['loop'] = loop
    
    test_map = {
        ('10k1k',       10000,      1000),
        #('mono',        10,         1,  1),
        #('hundred',     100,        15, 5),
        #('thousand',    1000,       15, 5),
        #('tthousand',   10000,      15, 5),
        #('hthousand',   100000,     15, 5),
        #('million',     1000000,    15, 5),
    }
    
    worker = DataPerf(**cfg)
    loop.create_task(worker.sanity_check())
    for test in test_map:
        if cfg['config']['cleanup']:
            loop.create_task(worker.cleanup(worker.name, test[0]))
        else:
            loop.create_task(worker.cycle(*test))

    worker.start()
