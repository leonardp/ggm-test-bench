import sys
import zmq
import asyncio
import logging

from ggm.lib.logging import GLog
from ggm.lib.kontext import Kontext

from time import sleep
from pprint import pprint

class TestKernel(GLog):
    """
    """
    def __init__(self, *args, **kwargs):
        #pprint(kwargs)
        self.loop = kwargs['loop']
        #self.context = kwargs['context'] or Kontext()
        self.context = Kontext()
        self.name = kwargs['name']
        self.version = kwargs['version']
        # Y U NO WORK???
        #super().__init__()
        GLog.__init__(self, *args, **kwargs)
        if kwargs['config']['verbose']:
            self.glog.setLevel(logging.DEBUG)

        sleep(2)

    def start(self, *args, **kwargs):
        self.loop.run_forever()

    def stop(self, *args, **kwargs):
        """
        TODO
        do a nice exit...
        """
        self.logger.info('Stopping Kernel {0} gracefully.'.format(self.name))
        self.ioloop.stop()
        sys.exit(0)
