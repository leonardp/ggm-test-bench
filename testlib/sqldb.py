import sqlite3
from pprint import pprint

class SqlDB(object):
    def __init__(self, dbname='data', *args, **kwargs):

        self.dbname = dbname
        self.db = sqlite3.connect(f'./{dbname}.db')

        cur = self.db.cursor()

        query = f'SELECT name FROM sqlite_master WHERE type="table"'
        cur.execute(query)
        self.measurements = [i[0] for i in cur.fetchall()]

        cur.close()

    def get_data(self, measurement, start='2000-01-01 00:00:00', stop=None, offset=0, limit=1000):
        cur = self.db.cursor()

        query = f'PRAGMA table_info({measurement})'
        cur.execute(query)
        records = cur.fetchall()
        clist = [i[1] for i in records]
        tlist = [i[2] for i in records]

        query = f'SELECT * FROM "{measurement}" WHERE time >= "{start}" '
        if stop:
            query = query+f"AND time <= '{stop}' "
        query = query+ f"ORDER BY time ASC LIMIT {limit} OFFSET {offset}"

        records = []
        cur.execute(query)
        records = cur.fetchall()

        records = self.from_db_format(records, clist, tlist)
        return records

    def get_iso_timestamp(self):
        return dt.utcnow().isoformat(sep=' ')

    def from_db_format(self, data, clist, tlist):
        """
        takes list of data tuples with values from data dict in order of key list
        returns list of data dicts with 'fields' key for float values and 'tags' key for strings
        TODO error checking?
        """
        for i, d in enumerate(data):
            tmp = {}
            tmp['fields'] = {}
            tmp['tags'] = {}
            for j, t in enumerate(tlist):
                if t == 'TIMESTAMP':
                    tmp[clist[j]] = d[j]
                elif t == 'FLOAT':
                    tmp['fields'].update({clist[j]: d[j]})
                else:
                    tmp['tags'].update({clist[j]: d[j]})
            data[i] = tmp
        return data
