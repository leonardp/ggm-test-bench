from multiprocessing import Process

import sys, zmq, syslog, logging, asyncio
from datetime import datetime

from ggm.lib.logging import GLog

class GTestManager(GLog):
    def __init__(self, name="GTestManager", cache=False, *args, **kwargs):
        self.loop = kwargs['loop']
        self.context = kwargs['context']
        self.kernel_map = kwargs['kernel_map']
        self.version = kwargs['version']
        self.name = name
        self.cache = cache

        # whyyyy??
        #super().__init__()
        GLog.__init__(self, *args, **kwargs)

    async def start_kernels(self):
        # start delayed wait for logger etc...
        await asyncio.sleep(1)
        for n, kernel in enumerate(self.kernel_map):
            self.init_kernel(kernel, n)
        return

    def init_kernel(self, kernel, n):
        self.kernel_map[n]['proc'] = Process(target=kernel['function'], args=(kernel,))
        self.kernel_map[n]['proc'].start()
        self.glog.info(f'Starting Kernel {kernel["name"]} with PID {self.kernel_map[n]["proc"].pid}')

    async def logger_task(self):
        """TODO: Fix logger formatting and message handling!!!!"""
        """TODO: filter logging for kernels (self.kernel_map) OR control kernel logging behavior!!!!"""
        s = self.context.socket(zmq.SUB)
        s.bind(self.LOG_DEST)
        s.subscribe(b'')
        while True:
            msg = await s.recv_multipart()
            #print(msg)
            level, message = msg
            message = message.decode('ascii')
            if message.endswith('\n'):
                message = message[:-1]
            log = getattr(logging, level.lower().decode('ascii'))
            syslog.syslog(message)

    def start(self):
        self.loop.create_task(self.logger_task())
        self.loop.create_task(self.start_kernels())

    def terminate(self):
        return
