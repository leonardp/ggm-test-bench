import os
import sys
import zmq
import socket
import asyncio
import zmq.auth
from zmq.auth.asyncio import AsyncioAuthenticator

from ggm.lib.kontext import Kontext

import random, string
from datetime import datetime as dt
from datetime import timezone as tz
from pprint import pprint

class CommonTest(object):
    def __init__(self, *args, **kwargs):
        keys_dir = os.path.join(kwargs['base_dir'], '../config')
        public_keys_dir = os.path.join(keys_dir, 'public_keys')
        secret_keys_dir = os.path.join(keys_dir, 'private_keys')
        client_secret_file = os.path.join(secret_keys_dir, f"{kwargs['config']['client_key']}.key_secret")

        self.client_public, self.client_secret = zmq.auth.load_certificate(client_secret_file)

        server_public_file = os.path.join(public_keys_dir, f"{kwargs['config']['server_key']}.key")
        self.server_public, _ = zmq.auth.load_certificate(server_public_file)

        self.target_ip = kwargs['config']['ip']

        self.ctx = Kontext()

    def get_zmq_client(self):
        client = self.ctx.socket(zmq.DEALER)

        client.curve_secretkey = self.client_secret
        client.curve_publickey = self.client_public
        client.curve_serverkey = self.server_public
        client.connect(f'tcp://{self.target_ip}:6001')
        return client

    async def cleanup(self, name, testname):
        ggm = self.get_zmq_client()

        cmd = ['series', 'insert', self.name, testname, {'time': self.get_iso_timestamp(), 'tags': {'deleteable': 'xxx'}, 'fields': {}}]
        await ggm.psnd(cmd)
        ret = await ggm.precv()

        cmd = ['series', 'delete', self.name, testname]
        await ggm.psnd(cmd)
        ret = await ggm.precv()
        self.glog.info(f'Cleaned up {self.name} - {testname}')

        cmd = ['json', 'delete', 'device_state_db', self.name]
        await ggm.psnd(cmd)
        ret = await ggm.precv()

        #cmd = ['json', 'delete', 'device_state_db', self.target_name, 'storage', f'{self.name}']
        #await ggm.psnd(cmd)
        #ret = await ggm.precv()

        self.glog.info(f'Cleaned up {self.name} - {testname}')

    async def sanity_check(self):
        ggm = self.get_zmq_client()
        while True:
            await asyncio.sleep(3)
            await ggm.psnd(['greetings'])
            resp = await ggm.precv()
            if 'earthlings' not in resp:
                self.glog.debug(f'Insane: {resp}!')
                sys.exit(1)

    def get_iso_timestamp(self):
        return dt.utcnow().isoformat(sep=' ')

    def gen_data(self, length=100, fcols=5, tcols=5):
        data = []
        for i in range(0,length):
            tmp = {
                'time': self.get_iso_timestamp(),
                'fields': {},
                'tags': {}
            }
            for j in range(0,fcols):
                tmp['fields'][f'fcol_{j}'] = round(random.uniform(0.5,1.5), 2)
            for j in range(0,tcols):
                tmp['tags'][f'tcol_{j}'] = ''.join(random.choice(string.printable) for x in range(random.randint(0,127)))

            data.append(tmp)
        return data

    def to_timestamp(self, dati):
        return dt.timestamp(dati.replace(tzinfo=tz.utc)) # whyyyyy

    def to_datetime(self, tmst):
        return dt.utcfromtimestamp(tmst) # oh whyyy

    def convert_to_timestamp(self, doc): # whyyy
        """
        recursive function
        converts values of specific keys in a nested dict
        from python datetime to timestamp (utc)
        horrible efficiency?
        """
        timestamp_keys = ['time', 'seen', 'head', 'tail', 'start', 'stop']

        if isinstance(doc, list):
            for thing in doc:
                self.convert_to_timestamp(thing)
        elif isinstance(doc, dict):
            for key in doc.keys():
                if isinstance(doc[key], dict) or isinstance(doc[key], list):
                    self.convert_to_timestamp(doc[key])
                elif key in timestamp_keys:
                    try:
                        doc[key] = self.to_timestamp(doc[key])
                    except Exception as e:
                        self.glog.error("Failed converting timestamp to datetime: {0}".format(e))
        return doc 

    def convert_to_datetime(self, doc): # whyyyyyyyy
        """
        recursive function
        converts values of specific keys in a nested dict
        from utc timestamps as floats to python datetime object
        horrible efficiency?
        """
        timestamp_keys = ['time', 'seen', 'head', 'tail', 'start', 'stop']

        if isinstance(doc, list):
            for thing in doc:
                self.convert_to_datetime(thing)
        elif isinstance(doc, dict):
            for key in doc.keys():
                if isinstance(doc[key], dict) or isinstance(doc[key], list):
                    self.convert_to_datetime(doc[key])
                elif key in timestamp_keys:
                    try:
                        doc[key] = self.to_datetime(doc[key])
                    except Exception as e:
                        self.glog.error("Failed converting timestamp to datetime: {0}".format(e))
        return doc


