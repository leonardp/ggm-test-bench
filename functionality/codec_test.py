import os, zmq, requests
import asyncio
from testlib.kernel import TestKernel
from testlib.common_test import CommonTest

import random
from datetime import timedelta as td

class CodecTest(TestKernel, CommonTest):
    def __init__(self, *args, **kwargs):
        TestKernel.__init__(self, *args, **kwargs)
        CommonTest.__init__(self, *args, **kwargs)

    async def check(self, cmd, expect):
        ggm = self.get_zmq_client()
        while True:
            await asyncio.sleep(random.uniform(1.5,2.5))
            await ggm.psnd(cmd)
            resp = await ggm.precv()

            if expect not in resp:
                self.glog.debug(f'command {cmd} expected {expect} but got: {resp}!')
                await asyncio.sleep(random.uniform(5,15))

    async def hammer(self):
        ggm = self.get_zmq_client()
        cmdlist = ['greetings', 'version', 'json', 'series']
        while True:
            for cmd in cmdlist:
                await asyncio.sleep(random.uniform(1.5,2.5))
                await ggm.psnd([cmd])
                resp = await ggm.precv()
                self.glog.debug(resp)


def codec_test_process(cfg):
    cfg['base_dir'] = os.path.dirname(__file__)

    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    cfg['loop'] = loop
    
    worker = CodecTest(**cfg)
    loop.create_task(worker.sanity_check())
    loop.create_task(worker.check(['greetings'], 'earthlings'))
    loop.create_task(worker.hammer())
    worker.start()
