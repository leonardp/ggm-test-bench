import os, zmq, requests
import asyncio
from testlib.kernel import TestKernel
from testlib.common_test import CommonTest

import copy, random
from datetime import timedelta as td

class HttpPoster(TestKernel, CommonTest):
    def __init__(self, *args, **kwargs):
        self.cleanup = kwargs['config']['cleanup']
        self.start_delay = (5,30)
        self.test_interval = (1,3)
        TestKernel.__init__(self, *args, **kwargs)
        CommonTest.__init__(self, *args, **kwargs)

    async def test_post(self, testname, length, fields, cols, tlimit):
        ggm = self.get_zmq_client()
        thresh = td(milliseconds=tlimit)

        if self.cleanup:
            cmd = ['series', 'insert', self.name, testname, {'time': self.get_iso_timestamp(), 'tags': {'deleteable': 'xxx'}, 'fields': {}}]
            await ggm.psnd(cmd)
            ret = await ggm.precv()

            cmd = ['series', 'delete', self.name, testname]
            await ggm.psnd(cmd)
            ret = await ggm.precv()
            self.glog.info(f'Cleaned up {self.name} - {testname}')

            cmd = ['json', 'delete', 'device_state_db', self.name]
            await ggm.psnd(cmd)
            ret = await ggm.precv()

            self.glog.info(f'Cleaned up {self.name} - {testname}')
            return

        await asyncio.sleep(*self.start_delay)
        self.glog.info(f'Starting {self.name} - {testname}')

        url = f'http://{self.target_ip}:5555/series/insert/{self.name}/{testname}'

        # update device state db for fun and profit
        cmd = ['json', 'nupsert', 'device_state_db', self.name, {'seen': self.get_iso_timestamp(), 'dev_id': self.name}]
        await ggm.psnd(cmd)
        ret = await ggm.precv()

        cmd = ['series', 'get', self.name, testname, 'head']

        while True:
            data = self.gen_data(length, fields, cols) 
            expected_data = {self.name: { testname: data[::-1] }}
            post_data = copy.deepcopy(data)
            #self.convert_to_timestamp(post_data)

            try:
                ret = requests.post(url=url, json=post_data.pop(0))
                if ret.elapsed > thresh:
                    self.glog.error(f'Looooooong http request {self.name}/{testname}: {ret.elapsed}')
            except Exception as e:
                self.glog.error(f'http post error: {e}')

            await ggm.psnd(cmd)
            ret = await ggm.precv()

            if not ret == expected_data:
                self.glog.error(f'name: {testname} - expected: {[k for k in expected_data.keys()]} - received: {[k for k in ret.keys()]}')
            await asyncio.sleep(*self.test_interval)


def http_post_process(cfg):
    cfg['base_dir'] = os.path.dirname(__file__)

    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    cfg['loop'] = loop
    
    test_map = {
        ('little_one',     1,   5,  5,  255),
        ('big_one',        1,   30, 30, 255),
        #('little_hundred', 100, 5,  5,  555),
        #('big_hundred',    100, 30, 30, 555),
    }
    
    worker = HttpPoster(**cfg)
    loop.create_task(worker.sanity_check())
    for test in test_map:
        if cfg['config']['cleanup']:
            worker.cleanup(worker.name, test)
        else:
            loop.create_task(worker.test_post(*test))

    worker.start()
