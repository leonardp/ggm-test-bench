import os, zmq, requests
import asyncio
from testlib.kernel import TestKernel
from testlib.common_test import CommonTest

import random
from datetime import timedelta as td
from pprint import pprint

class HttpGetter(TestKernel, CommonTest):
    def __init__(self, *args, **kwargs):
        self.start_delay = (5,30)
        self.test_interval = (2.5,3)
        TestKernel.__init__(self, *args, **kwargs)
        CommonTest.__init__(self, *args, **kwargs)

    async def test_get(self, testname, length, fields, cols, tlimit):
        ggm = self.get_zmq_client()
        thresh = td(milliseconds=tlimit)

        await asyncio.sleep(*self.start_delay)
        self.glog.info(f'Starting {self.name} - {testname}')

        data = self.gen_data(length, fields, cols)
        expected_data = {self.name: { testname: data[::-1] }}
        cmd = ['series', 'insert', self.name, testname, data]

        await ggm.psnd(cmd)
        ret = await ggm.precv()
        self.glog.debug(f'insert response: {ret}')

        # update device_state db
        cmd = ['json', 'nupsert', 'device_state_db', self.name, {'seen': self.get_iso_timestamp(), 'dev_id': self.name}]
        await ggm.psnd(cmd)
        ret = await ggm.precv()

        url = f'http://{self.target_ip}:5555/series/get/{self.name}/{testname}/all'
        while True:
            try:
                r = requests.get(url)
                #ret = self.convert_to_datetime(r.json())
                ret = r.json()
                if r.elapsed > thresh:
                    self.glog.error(f'Looooooong http request {self.name}/{testname}: {r.elapsed}')
            except Exception as e:
                self.glog.error(f'http get error: {e}')

            if not ret == expected_data:
                self.glog.error(f'name: {testname} - expected: {[k for k in expected_data.keys()]} - received: {[k for k in ret.keys()]}')
            await asyncio.sleep(*self.test_interval)


def http_get_process(cfg):
    cfg['base_dir'] = os.path.dirname(__file__)

    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    cfg['loop'] = loop

    test_map = {
        ('little_one',     1,    5,  5,  255),
        ('little_hundred', 100,  5,  5,  255),
        #('little_thou',    1000, 5,  5,  555),
        ('big_one',        1,    30, 30, 255),
        ('big_hundred',    100,  30, 30, 255),
        #('big_thou',       1000, 30, 30, 555),
    }
    
    worker = HttpGetter(**cfg)
    loop.create_task(worker.sanity_check())
    for test in test_map:
        if cfg['config']['cleanup']:
            loop.create_task(worker.cleanup(worker.name, test[0]))
        else:
            loop.create_task(worker.test_get(*test))
    worker.start()
