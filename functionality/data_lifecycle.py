import os, zmq, requests
import asyncio
from testlib.kernel import TestKernel
from testlib.common_test import CommonTest

import random
from datetime import timedelta as td
from pprint import pprint

class DataLifecycle(TestKernel, CommonTest):
    def __init__(self, *args, **kwargs):
        self.cleanup = kwargs['config']['cleanup']
        self.start_delay = (5,30)
        self.test_interval = (10,20)
        TestKernel.__init__(self, *args, **kwargs)
        CommonTest.__init__(self, *args, **kwargs)

    async def cycle(self, testname, length, fields, cols):
        ggm = self.get_zmq_client()

        if self.cleanup:
            cmd = ['series', 'insert', self.name, testname, {'time': self.get_iso_timestamp(), 'tags': {'deleteable': 'xxx'}, 'fields': {}}]
            await ggm.psnd(cmd)
            ret = await ggm.precv()

            cmd = ['series', 'delete', self.name, testname]
            await ggm.psnd(cmd)
            ret = await ggm.precv()
            self.glog.info(f'Cleaned up {self.name} - {testname}')

            cmd = ['json', 'delete', 'device_state_db', self.name]
            await ggm.psnd(cmd)
            ret = await ggm.precv()

            self.glog.info(f'Cleaned up {self.name} - {testname}')
            return

        await asyncio.sleep(*self.start_delay)
        self.glog.info(f'Starting {self.name} - {testname}')

        data = self.gen_data(length, fields, cols)
        expected_data = {self.name: { testname: data[::-1] }}

        # update device_state db
        cmd = ['json', 'nupsert', 'device_state_db', self.name, {'seen': self.get_iso_timestamp(), 'dev_id': self.name}]
        await ggm.psnd(cmd)
        ret = await ggm.precv()

        while True:
            # upload large data as chunks
            if length > 2000:
                raise Exception('data size too large -- not implemented -> performance testing')
            else:
                cmd = ['series', 'insert', self.name, testname, data]
                await ggm.psnd(cmd)
            ret = await ggm.precv()
            #self.glog.debug(f'{testname} inserted {length} points --- {ret}')
            await asyncio.sleep(*self.test_interval)

            # get head
            cmd = ['series', 'get', self.name, testname, 'head']
            await ggm.psnd(cmd)
            ret = await ggm.precv()
            if not ret[self.name][testname][0] == data[-1]:
                self.glog.error(f'{self.name} - {testname} --- error getting head!')

            # get tail
            cmd = ['series', 'get', self.name, testname, 'tail']
            await ggm.psnd(cmd)
            ret = await ggm.precv()
            if not ret[self.name][testname][0] == data[0]:
                self.glog.error(f'{self.name} - {testname} --- error getting tail!')

            # get all
            if length > 2000:
                ret = await ggm.download(['series', 'chunk', self.name, testname], {'start': data[0]['time'], 'stop': data[-1]['time']})
            else:
                cmd = ['series', 'get', self.name, testname, 'all']
                await ggm.psnd(cmd)
                ret = await ggm.precv()

            if not ret == expected_data:
                self.glog.error(f'{self.name} - {testname} --- error getting all!')

            ## get some (fuck this)
            #cmd = ['series', 'get', self.name, testname, {'start': data[0]['time'], 'stop': data[int(length*0.2)]['time']}]
            #await ggm.psnd(cmd)
            #ret = await ggm.precv()
            #if not ret[self.name][testname] == expected_data[self.name][testname][0:int(length*0.2)]:
            #    print('ERROR - get some')
            #    pprint(ret[self.name][testname])
            #    pprint(data[-int(length*0.2)::-1])
            #    pprint(expected_data[self.name][testname][:-int(length*0.2)])

            # failed delete
            cmd = ['series', 'delete', self.name, testname]
            await ggm.psnd(cmd)
            ret = await ggm.precv()
            #if ret[self.name][testname]['Error']:
            if not ret['Error']:
                self.glog.error(f'{self.name} - {testname} --- faild failing delete: {ret}!')

            # failed delete
            cmd = ['series', 'insert', self.name, testname, {'time': '2000-01-01 00:00:00', 'tags': {'deleteable': 'xxx'}, 'fields': {}}]
            await ggm.psnd(cmd)
            ret = await ggm.precv()
            cmd = ['series', 'delete', self.name, testname]
            await ggm.psnd(cmd)
            ret = await ggm.precv()
            if not ret == {'Error': True, 'Reason': f'{testname} is not deleteable.' }:
                self.glog.error(f'{self.name} - {testname} --- faild failing delete: {ret}!')
            
            # delete
            cmd = ['series', 'insert', self.name, testname, {'time': self.get_iso_timestamp(), 'tags': {'deleteable': 'xxx'}, 'fields': {}}]
            await ggm.psnd(cmd)
            ret = await ggm.precv()

            cmd = ['series', 'delete', self.name, testname]
            await ggm.psnd(cmd)
            ret = await ggm.precv()
            if not ret == {'Error': False, 'Reason': f'Deletion of {testname} successful.'}:
                self.glog.error(f'{self.name} - {testname} --- failed deleting: {ret}!')

            cmd = ['series', 'get', self.name, testname, 'head']
            await ggm.psnd(cmd)
            ret = await ggm.precv()
            if not ret[self.name][testname]['Error']:
                self.glog.error(f'{self.name} - {testname} --- table not deleted: {ret}!')

            await asyncio.sleep(*self.test_interval)


def data_lifecycle_process(cfg):
    cfg['base_dir'] = os.path.dirname(__file__)

    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    cfg['loop'] = loop
    
    worker = DataLifecycle(**cfg)
    loop.create_task(worker.sanity_check())
    test_map = {
        ('mono',       10,         1,  1 ),
        ('hundred',    100,        15,  5),
        ('thousand',   1000,       15,  5),
        #('tthousand',  10000,      15,  5),
        #('hthousand',  100000,     15,  5),
        #('million',    1000000,    15,  5),
    }

    for test in test_map:
        if cfg['config']['cleanup']:
            loop.create_task(worker.cleanup(worker.name, test[0]))
        else:
            loop.create_task(worker.cycle(*test))

    worker.start()
